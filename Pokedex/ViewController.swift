//
//  ViewController.swift
//  Pokedex
//
//  Created by COTEMIG on 21/02/22.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad - A tela foi carregada")
        // Do any additional setup after loading the view.
        
        let button = UIButton()
        button.layer.cornerRadius = 15
        
    }
    
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear - A tela vai ser exibida")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear - A tela foi exibida")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear - A tela vai desaparecer")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear - A tela desapareceu")
    }


}

